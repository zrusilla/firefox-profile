package Firefox::Extension;

use strict;
use warnings;

use Data::Dumper;
use Mouse;
use File::Copy qw(move);
use File::Path qw(make_path remove_tree);
use File::Temp   qw(tempdir);
use File::Spec::Functions qw(catfile);
use Firefox::Profile::Constants;
use List::Util qw(first max);
use XML::Tiny qw(parsefile);

has xpi             => (is => 'rw', isa => 'Str', required => 1);
has unzip           => (is => 'rw', isa => 'Str', required => 1, default => UNZIP);
has min_firefox     => (is => 'rw', isa => 'Str');
has max_firefox     => (is => 'rw', isa => 'Str');
has app_id          => (is => 'rw', isa => 'Str');
has temp_dir        => (is => 'rw', isa => 'Str');
has install_dir     => (is => 'rw', isa => 'Str');
has firefox_version => (is => 'rw', isa => 'Str');

sub BUILD {
    my $self = shift;
    
    make_path($self->install_dir);
    
    $self->temp_dir(
        tempdir( DIR => $self->install_dir, CLEANUP => 1 )
    );
    
    my $cmd    = join ' ', $self->unzip,  '-d', $self->temp_dir,  $self->xpi;
    my $retval =  qx {$cmd};
    
    my $manifest = catfile($self->temp_dir,'install.rdf');
    
    {
        # Cleanup.
        local $/ = undef;
        open my $fh, '<', $manifest
          or die "Can't open $manifest for reading: $!";
        my $contents = <$fh>;
        close $fh or die "Can't close $manifest: $!";

        #  The byte-order mark makes XML::Tiny unhappy.
        $contents =~ s/\357\273\277//g;

        # DOS-> Unix
        $contents =~ s/\r//g;

        open $fh, '>', $manifest
          or die "Can't open $manifest for writing: $!";
        print $fh $contents;
        close $fh or die "Can't close $manifest: $!";
    }

    # Get the application ID from the XML.

    my $rdf = parsefile($manifest);

    my ($app_id) = 
      map { $_->{content}->[0]->{content} }
      grep { $_->{name} eq 'em:id' }
      @{ $rdf->[0]->{content}->[0]->{content} };

    my @description = @{$rdf->[0]->{content}->[0]->{content}};
    
    my @target_applications =
      map { $_->{content}->[0]->{content} }
      grep { $_->{name} eq 'em:targetApplication' }
      @description
    ;
      
      
    my $firefox;
    if (@target_applications == 1) {
        $firefox = shift @target_applications;
        #code
    }
    else {
        ($firefox) = first {
             $_->[0]->{content}->[0]->{content} eq FIREFOX_GUID
          || $_->[0]->{content}->[0]->{content} eq TOOLKIT_ID
        } @target_applications;
    }

    my ( $min_ff ) =
      map { $_->{content}->[0]->{content} =~ /^(\d+\.?\d*)/ }
      grep { ( $_->{name} eq 'em:minVersion' ) } @{$firefox};
      
    my ( $max_ff ) =
      map { $_->{content}->[0]->{content} =~ /^(\d+\.?\d*)/ }
      grep { ( $_->{name} eq 'em:maxVersion' ) } @{$firefox};
      
    $self->app_id($app_id);
    $self->min_firefox($min_ff) if $min_ff;
    $self->max_firefox($max_ff) if $max_ff;
    
    return $self;
}

sub install {
    my $self = shift;
    
    my %arg = @_;
    
    my $install_dir = $arg{install_dir} || $self->install_dir;
    die "No installation directory!" unless $install_dir;
    
    $install_dir = catfile ($install_dir, $self->app_id); 

    my $success = move( $self->temp_dir, $install_dir);


}


1;
