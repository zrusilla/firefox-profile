package Firefox::Preferences;

use strict;
use warnings;
use feature ':5.10';

use Mouse;

use File::Read;
use File::Spec::Functions qw(catfile);

has '_pref'           => (is => 'rw', isa => 'HashRef' );
has 'firefox_version' => (is => 'rw', isa => 'Str'     );                        
has 'files'           => (is => 'rw', isa => 'ArrayRef');
has 'verbose'         => (is => 'rw', isa => 'Bool', default => 0);                        
has 'no_check'        => (is => 'rw', isa => 'Bool', default => 0);

my @valid_types = qw(bool int string);

sub BUILD {
    my $self = shift;
    $self->_pref({});
    $self->parse  if $self->files;
    return $self;
}

sub parse {
    
    my $self = shift;
    my @files = (@_ ||  @{$self->files});
    
    my @prefs;
    foreach my $prefs_js ( @files ) {
        push @prefs, read_file({  skip_blanks => 1, aggregate => 0, } ,$prefs_js) if -e $prefs_js;
    }
    
    my $pref = $self->_pref || {};
    
    foreach my $line (@prefs) {
        next unless defined $line ;
        next if $line =~ m{^\/\/};   # comments
        
        my ($name, $value, $type) = parse_pref($line);
        
        if ($self->verbose && exists $pref->{$name}) {
            warn "$name already exists: " . join ' ', @{$pref->{$name}}
              .  " Replacing with $value ($type)" 
            ;
        }
        $pref->{$name} = [$value, $type];
    }
    if ($self->no_check ) {
            
        # Turn off compatibility checking for extensions.
        my $version = $self->firefox_version;
        my @version = split /\./, $version;
        
        my $compat_pref = 'extensions.checkCompatibility';
        foreach my $part (@version) {
            $compat_pref = join '.', $compat_pref, $part;
            $pref->{$compat_pref} = ['false', 'bool'];
        }
    }
    $self->_pref($pref);
}
    
    
sub parse_pref {
    my $line = shift;
    
    my ($pref, $value) = $line =~ /user_pref\("(.*)",\s*([^)]+)\);/;
        
    $value =~ s/\s+$//g; #rtrim
        
    # TODO: What about floats?
    my $type = determine_type($value);
    $value =~ s/['"]//g if $type eq 'string'; # ditch the quotes
    
    return ($pref, $value, $type);
}

sub prefs_to_string {
    my $self = shift;
    
    my $pref = $self->_pref;
    
    my @prefs;
    
    foreach my $name (sort keys %$pref) {
        my ($value, $type) = @{$pref->{$name}};
        
        my $str =
            qq{user_pref("$name",}
          . format_value($value, $type)
          . ');'
        ;
        push @prefs, $str;
        
    }
    return wantarray ? @prefs : join "\n", @prefs;
}

sub set_pref {
    my ($self, %arg ) =  @_;
    
    die "No pref specified!" unless $arg{name};
    die "No pref value!"     unless $arg{value};
    die "No pref type!"      unless $arg{type};
    
    return $self->_pref->{$arg{name}} = [$arg{value}, $arg{type}];
}

sub determine_type {
    my $value = shift;
    # TODO: What about floats?
    return
        $value eq 'true'  ? 'bool'
      : $value eq 'false' ? 'bool'
      : $value =~ /^\d+$/ ? 'int'
      : $value =~ /^['"].*['"]$/ ? 'string'
      : undef
    ; 
}

sub format_value {
    my ($value, $type) = @_;
    return $type eq 'string' ? qq{"$value"} : $value
}


sub write_prefs {
    my ($self , $profile_dir) = @_;
    
    my @prefs = $self->prefs_to_string();
    
    my $fh;
    my $prefs = catfile( $profile_dir, 'prefs.js' );
    
    open  $fh, '>', $prefs or die "Can't open $prefs for writing: $!";
    print $fh map { "$_\n" } @prefs; 
    close $fh or die "Can't close $prefs: $!";
    
}

1;

=head1 NAME

Firefox::Profiles::Preferences - Perl module to read, parse and output preferences.

=head1 METHODS


=head2 write_prefs

Write combined preferences to profile's prefs.js file.

=cut

__DATA__
---
firefox_prefs :  
 - app.update.enabled : false
 - browser.rights.2.shown : true
 - browser.rights.3.shown : true
 - browser.rights.4.shown : true
 - browser.sessionstore.enabled : false
 - browser.sessionstore.resume_from_crash : false
 - browser.shell.checkDefaultBrowser : false
 - browser.startup.homepage_override.mstone : ignore
 - browser.startup.page : 0 
 - extensions.autoDisableScopes : 0
 - extensions.enabledAddons : 
 - extensions.update.autoUpdateDefault : false
 - extensions.update.enabled : false
 - network.http.phishy-userpass-length : 255
 - plugins.hide_infobar_for_missing_plugin : true
 - plugins.hide_infobar_for_outdated_plugin : true
 - security.warn_entering_weak : false
 - security.warn_entering_weak.show_once : false
 - security.warn_viewing_mixed : false
 - security.warn_viewing_mixed.show_once : false
 - signon.rememberSignons : false
 - toolkit.telemetry.prompted : 2
 - toolkit.telemetry.rejected : true
firebug_prefs :
 - extensions.firebug.net.enableSites : true
 - extensions.firebug.allPagesActivation : on
 - extensions.firebug.showFirstRunPage : false
 - extensions.firebug.addonBarOpened : true
 - extensions.firebug.previousPlacement : 3
