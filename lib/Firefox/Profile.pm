package Firefox::Profile;

use strict;
use warnings;

use Mouse; # keep it lite

use AnyEvent;
use AnyEvent::Util;
use FindBin '$Bin';
use File::Basename qw(dirname);
use File::Path     qw(make_path remove_tree);
use File::Spec::Functions qw(catfile);
use Firefox::Preferences;
use Firefox::Extension;
use Firefox::Profile::Constants;
use Firefox::ProfilesIni;
use POSIX qw(getpwuid);
use IPC::Open3;

my %valid_types = ( bool => 1, int => 1, string => 1);  

has 'name'        => (is => 'rw', isa => 'Str', );
has 'profiles_ini'   => (is => 'rw', isa => 'Str', default => PROFILES_INI);
has 'profile_dir'    => (is => 'rw', isa => 'Str', default => PROFILE_DIR );

has 'firefox'         => (is => 'rw', isa => 'Str', default => FIREFOX);

# TODO: setter method
has 'firefox_version' => (is => 'rw', isa => 'Str', );

has 'extensions'      => (is => 'rw', isa => 'ArrayRef');
has 'preferences'     => (is => 'rw', isa => 'ArrayRef');

has 'display'         => (is => 'rw', isa => 'Str', default => DISPLAY);
has 'unix_user'       => (is => 'rw', isa => 'Str');

has 'unzip'           => (is => 'rw', isa => 'Str', default => UNZIP);

has 'no_check'        => (is => 'rw', isa => 'Bool', default => 0);
has 'lock'            => (is => 'rw', isa => 'Bool', default => 0);

has 'ini'             => (is => 'rw', isa => 'Firefox::ProfilesIni');
has 'prefs'           => (is => 'rw', isa => 'Firefox::Preferences');

our $VERSION = '0.03';

# extension information: min and max FF version, app id
my %ext = ();
    
sub BUILD {
    
    my $self = shift;
    
    die 'No Firefox at ' . $self->firefox unless -x $self->firefox;
    
    
    $self->firefox_version(get_ff_version($self->firefox));
    
    return $self;
}

sub install {
    my ($self, %arg) = @_;
    
    die 'No profile directory!' unless $self->profile_dir;

    $self->check_prerequisites;
    
    # wipe the slate clean.
    remove_tree($self->profile_dir);
    make_path($self->profile_dir);

    $self->create_profile;
    $self->install_preferences($arg{override_prefs});
    $self->install_extensions;
}

sub get_ff_version {
    my $ff  = shift;
    my $dir = dirname($ff);
    local $ENV{PATH} = $dir;
    my $output = qx { $ff -v };
    my ($ff_version) = $output =~ /Mozilla Firefox (\d+(?:\.\d+)+)/;
    return $ff_version;
}



sub check_prerequisites {
    my ($self) = @_;

    # Do we have Firefox?
    die $self->firefox . ' not found' unless -x $self->firefox;
}

#
# Create profile modifies profiles.ini and creates the profile subdir
#
sub create_profile {
    my $self = shift;

    my $profile  = $self->name; #profile name from profile obj
    my $prof_dir = $self->profile_dir;
    
    make_path $prof_dir;
    
    my $ini = Firefox::ProfilesIni->new(file => $self->profiles_ini);
    
    $ini->delete(profile => $profile);
    $ini->write_ini;
    
    my $cmd = join ' ',
        #'DISPLAY='. $self->display,      # X display
        $self->firefox,                  # Firefox
        '-no-remote -createProfile',     # Create profile
        qq{"$profile $prof_dir"},        # Where to create it
    ;
    
    my $unix_user = $self->unix_user;
    
    # Install as somebody else
    if ($self->unix_user) {
        my $real_user = (getpwuid($>))[0];
        
        # Run Firefox as the desired target user in order to edit
        # the appropriate profiles.ini.
        if ($unix_user ne $real_user) {
            $cmd = "su -l $unix_user -c '$cmd'";
        }
    }
    
    local $ENV{DISPLAY} = $self->display;
    my $cmd_cv = run_cmd(
        $cmd,
        '<'  => '/dev/null',
        '>'  => \my $out,
        '2>' => \my $err,
    );
    $cmd_cv->recv;

    die 'Could not generate Firefox profile ' . $self->name . ": $err" if $err!~ /Success/;
    
}

sub install_preferences {
    my ($self, $override_prefs)  = @_;

    
    $self->prefs( 
        Firefox::Preferences->new(
            files           => $self->preferences,
            firefox_version => $self->firefox_version,
            no_check        => $self->no_check,
        ) 
    );
    
    foreach my $pref (@$override_prefs) {
        die "Override pref: specify name, value, type" unless @$pref == 3;
        die "Invalid type: $pref->[2]" unless $valid_types{$pref->[2]};
        $self->prefs->set_pref(
            name  => $pref->[0],
            value => $pref->[1],
            type  => $pref->[2],
        );
    }
    $self->prefs->write_prefs($self->profile_dir);
}

sub install_extensions {
    my $self = shift;

    my $install_dir = catfile( $self->profile_dir, 'extensions' );

    foreach my $xpi ( @{$self->extensions} ) {
        
        my $ext =  Firefox::Extension->new(
            xpi         => $xpi,
            install_dir => $install_dir,
            firefox_version  => $self->firefox_version,
        );
        $ext->install;
    } 
}

=head1 NAME

Firefox::Profile - generate Firefox profiles with extensions and preferences.

=head1 SYNOPSIS

In a script

    my $profile = Firefox::Profile->new(
        name => 'myprofile',
        extensions => [
            '/path/to/extension.xpi'
        ],
        preferences => [
            '/path/to/prefs1.js',
            '/path/to/prefs2.js'
        ]
    );
    
    # or
    
    my $profile = Firefox::Profile->new;
    $profile->name('myprofile');
    $profile->extensions(\@extensions);
    $profile->preferences(\@preferences);
    
    # then
    
    $profile->install;

Module::Build action    

    sub ACTION_profiles {
        my ($self, %arg) = @_;
        my $ff = Firefox::Profile->new(%arg);
        $ff->install;
    }
    
    ./Build profiles
    
Once your profile is installed, you can use it with

    $ /path/to/firefox -P myprofile

=head1 DESCRIPTION

Firefox::Profile generates Firefox profiles.
It edits profiles.ini, creates a profile, and installs extensions
and preferences.

Regenerating profiles regularly keeps automated Firefox applications
in good working order.

A set of suggested preferences useful for automating Firefox (and Firebug)
is supplied but not installed by default.

=head1 PARAMETERS

These may be set in new() as key => value pairs, or using individual accessors.

=head2 Required

=over

=item profile

Name of profile to create.

=back

=head2 Optional 

=over

=item display

X display. Defaults to envvar C<$DISPLAY> or, as a last resort, C<:0>.
Useful in conjunction with Xvfb, the headless X server.

=item firefox

Location of Firefox.  Defaults to C</usr/bin/firefox>.

=item no_check

Tell Firefox not to check for extension compatibility.

=item  profiles_ini

Location of profiles.ini file.  Defaults to C<$HOME/.mozilla/firefox/profiles.ini> .
The Pale Moon browser's profiles.ini file is located at 

=item extensions

List of extensions to install. Arrayref containing full path names of XPI files. Example:

    extensions => [ '/my/tmpdir/mozrepl.xpi', '/my/tmpdir/firebug.xpi' ]

=item preferences

List of preferences files (.js)  Arrayref containing full path names of .js files.

=item profile_dir

Location to install the generated profiles.  Defaults to $HOME/.mozilla/firefox .

=item unix_user

To install as someone other than the present user. You'll need su permissions for that.

=item unzip

Location of unzip utility.  Default is /usr/bin/unzip.

=back

=head1 OTHER METHODS

=head2 install

Install the profile!

=over

=item * Edit profiles.ini

=item * Create profile

=item * Install extensions

=item * Install preferences

=back

=head2 create_profile

Invoke Firefox with options to silently create the profile.

=head2 install_extensions

Install the extensions into the profile directory.

=head2 install_preferences

Install the preferences into the profile directory.

=head1 PREPARATION

=over

=item * If necessary, locate and install the version of Firefox desired.

=item * Locate and download the extensions as XPI files. 

=item * Create a file of custom preferences, if any

=item * Give your new profile a name

=back

=BUGS

Without a doubt. I anxiously await your feedback.

=head1 AUTHOR

Elizabeth Cholet, C<< <zrusilla at yahoo.com> >>

=head1 COPYRIGHT

=head1 LICENSE

Copyright (C) 2014 by Elizabeth Cholet

This package is free software; you can redistribute it and/or modify it under
the terms of the original Artistic License, as published by Larry Wall,
or the Artistic License Version 2.0 - http://www.perlfoundation.org/legal/licenses/artistic-2_0.html,
or at your option any later version of it.

Have fun.

=cut

1;

__DATA__
[General]
StartWithLastProfile=1

[Profile0]
Name=myprofile
IsRelative=0
Path=/path/to/profile
