package Firefox::Profile::Constants;

use strict;
use warnings;

use File::Spec::Functions qw(catdir);
use Exporter qw(import);

our @EXPORT = qw(
    DOT_MOZILLA
    DISPLAY
    FIREFOX
    PROFILE_DIR
    PROFILES_INI
    UNZIP
    FIREFOX_GUID
    TOOLKIT_ID
);

use constant {
    DOT_MOZILLA   => catdir($ENV{HOME}, '.mozilla'),
    DISPLAY       => $ENV{DISPLAY} || ':0',
    FIREFOX       => '/usr/bin/firefox',
    UNZIP         => '/usr/bin/unzip',
    FIREFOX_GUID  =>  '{ec8030f7-c20a-464f-9b0e-13a3a9e97384}',
    TOOLKIT_ID    => 'toolkit@mozilla.org',

};

use constant PROFILE_DIR  => catdir(DOT_MOZILLA, 'firefox');
use constant PROFILES_INI => catdir(PROFILE_DIR, 'profiles.ini');

1;

