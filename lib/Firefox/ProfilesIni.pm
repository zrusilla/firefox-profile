package Firefox::ProfilesIni;

use strict;
use warnings;

use Mouse;

use Config::IniFiles;
use File::Spec::Functions qw(catdir catfile);
use File::Basename qw(dirname);
use File::Path 'make_path';
use File::Read;
use Firefox::Profile::Constants;
use List::Util 'max';

has file =>(is => 'rw', isa => 'Str', default => PROFILES_INI);
has ini  =>(is => 'rw', isa => 'Config::IniFiles');

sub BUILD {
    my $self = shift;
    $self->ini($self->read_ini);
    return $self;
}

=head2 new

Attributes:

=over

=item * file

Full pathane to profiles.ini file

=item * ini

Config::IniFiles object.

=back

=head2 read_ini

Read the ini file and create a Config::IniFiles object from it.

If none is found, will create a Config::IniFiles object with
the rudiments of a profiles.ini file:

    [General]
    StartWithLastProfile=1
    
=cut

sub read_ini {
    my $self = shift;
    
    my $profile_ini_dir = dirname( $self->file);
    
    make_path $profile_ini_dir;

    # Create a new profiles.ini if it doesn't exist.
    if ( !-f $self->file) {
        {
            local $/ = undef;
            my $prof_ini = $self->file;
            
            my $pos = tell DATA;
            
            my $prof = <DATA>;
            open my $fh, '>', $prof_ini or die "Can't open $prof_ini: $!";
            print $fh $prof;
            close $fh  or die "Can't close $prof_ini: $!";
            
            seek DATA, $pos, 0;
        }
    }

    my $ini;
    eval { $ini = Config::IniFiles->new( -file => $self->file); };
    if ($@) {
        die 'No profiles.ini ';
    }
    
    return $ini
}


=head2 get_profile_section

Argument: $profile_name
Returns: ini file section.

Given a profile name ("myprofile", "FooBar.default", etc.), returns
the corresponding ini file section, eg. [Profile1].

=cut



sub get_profile_section {
    my ($self, $profile) = @_;
    return undef unless $profile;
    
    foreach my $section ( $self->ini->Sections ) {
        next if $section  eq 'General';
        return $section if  $self->ini->val( $section, "Name") eq $profile;
    }
    return; 
}

=head2 get_profile_name

Argument: ini file section.
Returns: profile name

The opposite of get_profile_section: given a section name, return the profile
name.

=cut

sub get_profile_name {
    my ($self, %arg) = @_;
    
    return $self->ini->val( $arg{section}, "Name");
}

=head2

    $ini->add(
        name => "test_profile",
        path => "$Bin/blib"
    );
   
Note: Generating profiles with Firefox itself creates the profile
and modifies the profiles.ini at the same time.

=cut

sub add {
    my ($self, %arg) = @_;
    
    if (my $section = $self->get_profile_section($arg{profile})) {
        return $section;
    }
    
    my $max = $self->max_profile_section;
    my $section = "Profile$max";
    
    my $rel = $arg{relative} ? 1 : 0;
    
    $self->ini->AddSection( $section );
    
    $self->ini->newval ($section, 'Name'       => $arg{profile});
    $self->ini->newval ($section, 'IsRelative' => $rel);
    $self->ini->newval ($section, 'Path'       => $arg{path});
    if ($arg{default}) {
        my $default = $arg{default} ? 1 : 0;
        $self->ini->newval ($section, 'Default' => $default);
    }
    
    return $section;
}

sub max_profile_section {
    my ($self) = @_;
    my $max = max map {
        $_ =~ s/^Profile//
    } grep /Profile/, $self->ini->Sections;
    return defined $max ? $max : '0';
}

=head2 delete

Argument: profile => $profile_name OR section => $section_name
Returns: Section deleted, if any

Where both are specified, section name takes precedence.

=cut

sub delete {
    
    my ($self, %arg) = @_;
    my $section = $arg{section} ||  $self->get_profile_section(  $arg{profile} );
    
    return unless $section;
    
    
    return $self->ini->DeleteSection( $section );
}

=head2 get_path

Argument: profile => $profile_name OR section => $section_name
Returns: Path to Firefox profile.

=cut

sub get_path {
    my ($self, %arg) = @_;
    my $section = $arg{section} ||  $self->get_profile_section( profile => $arg{profile} );
    
    return $self->ini->val( $section, 'Path'  );
}

=head2 write_ini

Write the profiles.ini file back out to file.

=cut

sub write_ini {
    
    my ($self, $file) = @_;
    
    $file ||= $self->file;
    
    my $retval = eval {
        $self->ini->WriteConfig($file);
    };
    
    if ($@ or ! defined $retval) {
        warn "Failed to write ini file: $@"
    }
    
}

__PACKAGE__->meta->make_immutable();

1;


__DATA__
[General]
StartWithLastProfile=1

