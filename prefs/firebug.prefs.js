// Suggested Firebug prefs for automated applications
user_pref("extensions.firebug.net.enableSites", true);
user_pref("extensions.firebug.allPagesActivation", "on");
user_pref("extensions.firebug.showFirstRunPage", false);
user_pref("extensions.firebug.addonBarOpened", true);
user_pref("extensions.firebug.previousPlacement", 3);
//user_pref("extensions.firebug.alwaysOpenTraceConsole", true);
