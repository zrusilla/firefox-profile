use Test::More;

use strict;
use warnings;

use_ok('Firefox::Preferences');

my $prefs = Firefox::Preferences->new;

isa_ok($prefs, 'Firefox::Preferences');

my $retval = $prefs->set_pref(
    name  => 'foo',
    value => 'bar',
    type  => 'string'
);

my $href = $prefs->_pref;

isa_ok($href->{foo}, 'ARRAY');
is($href->{foo}->[0], 'bar', 'value');
is($href->{foo}->[1], 'string', 'type');

done_testing;