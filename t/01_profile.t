#!perl

use strict;
use warnings;
use FindBin '$Bin';
use File::Path 'remove_tree';
use File::Spec::Functions qw(catfile);
use Firefox::ProfilesIni;
use Test::More ;

use_ok('Firefox::Profile');

my $dat_dir      = "$Bin/dat";
my $profile      = 'testprofile';
my $profile_dir  = "$dat_dir/testprofile";
my $profiles_ini = "$dat_dir/profiles.ini";

my @extensions  = <$dat_dir/*.xpi>;

my @preferences =  <$dat_dir/*.js>;
unshift @preferences, "$Bin/../prefs/firefox.prefs.js";
unshift @preferences, "$Bin/../prefs/firebug.prefs.js";

remove_tree ($profile_dir) if -d $profile_dir;

my $prof = Firefox::Profile->new(
	name         => $profile,
	profile_dir  => $profile_dir,  # where to install everything
	profiles_ini => $profiles_ini, # location of profiles.ini
	extensions   => \@extensions,  # extensions to install
	preferences  => \@preferences, # prefs to install
);

$prof->no_check(1);

isa_ok( $prof, 'Firefox::Profile' );

is ($prof->name, $profile, 'name');
is ($prof->profile_dir  ,$profile_dir, 'profile_dir');  
is ($prof->profiles_ini ,$profiles_ini, 'profiles_ini');
is_deeply($prof->extensions,  \@extensions, 'extensions'); 
is_deeply($prof->preferences, \@preferences, 'preferences'); 

SKIP : {
	skip "No tests involving a live Firefox", 2 unless $ENV{FP_TEST_FIREFOX};
	$prof->install;

	ok( -d $profile_dir );
	ok( -d "$profile_dir/extensions" );
	remove_tree($profile_dir);
}

done_testing;
