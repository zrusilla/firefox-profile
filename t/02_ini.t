#!perl

use Test::More;
use FindBin '$Bin';
use File::Read 'err_mode=quiet';
use File::Spec::Functions qw(catfile);

use_ok('Firefox::ProfilesIni');

my $dat_dir      = "$Bin/dat";
my $profile      = 'testprofile';
my $profile_dir  = "$dat_dir/testprofile";
my $profiles_ini = "$dat_dir/profiles.ini";

unlink $profiles_ini;

my $ini = Firefox::ProfilesIni->new( file => $profiles_ini );

isa_ok( $ini,      'Firefox::ProfilesIni' );
isa_ok( $ini->ini, 'Config::IniFiles' );
is( $ini->file, $profiles_ini, $profiles_ini );

my $new_section = $ini->add(
    profile => $profile,
    path    => $profile_dir,
);

$ini->write_ini($profiles_ini);

is( $ini->get_profile_section($profile), $new_section, 'get_profile_section' );

ok( $ini->delete( profile => $profile ), "delete $profile from profiles.ini" );

is( $ini->get_profile_section($profile),  undef, "$profile is gone" );

$ini->write_ini;

unlink $profiles_ini;

done_testing;
