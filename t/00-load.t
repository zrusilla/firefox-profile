#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Firefox::Profile' ) || print "Bail out!
";
}

diag( "Testing Firefox::Profile $Firefox::Profile::VERSION, Perl $], $^X" );
